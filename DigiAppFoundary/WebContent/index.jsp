<%@page import="DigiAppFoundaryDB.Order"%>
<%@page import="java.sql.SQLException"%>
<%@page import="DigiAppFoundaryDB.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DigiAppFoundaryDB.DisplayStock"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
	crossorigin="anonymous"></script>
<title>Document</title>


<style>
.btn-primary {
	color: #fff;
	background-color: #7ca2c5;
	border-color: #0d6efd;
	margin: 0px 5px 0px 0px;
}

.cont {
	background-color:#7ca2c5;
	width: 200px;
	border: 1px solid grey;
	border-radius: 5px;
	padding: 0px;
	margin-left: 20px;
	height: 100px;
}

.container2, h1 {
	font-size: large;
}

.cont0 {
	margin-top: 20px;
}
</style>
</head>
<body>
	<%
		DisplayStock display = new DisplayStock();
	%>
	<div class="container cont0">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 cont">

					<%
						Integer Categories = display.getTotalCategories();
					%>

					<h1>Total Categories</h1>
					<p><%=Categories%></p>

				</div>
				<div class="col-sm-3 cont">
				<%
				   Integer totalItem = display.getTotalItems();
				%>
					<h1>Total items</h1>
					<p><%= totalItem %></p>
				</div>
				<div class="col-sm-3 cont">
				<%
				
				ArrayList<String> maxItems = display.getMaxItems(); 
				%>
					<h1>Top Selling Category</h1>
					<p><%= maxItems.get(0)%></p>
					
				</div>
				<div class="col-sm-3 cont">
					<h1>Total Selling item</h1>
					<p><%= maxItems.get(1) %></p>
				</div>
			</div>
		</div>
		<br>
		<div class="container">
			<div class="jumbotron">
				<div class="card">
					<h5 class="card-header" style="font-family: auto;">Stock
						Management</h5>
					<div class="card-body">
						<table class="table table-striped">
							<thead>
								<tr
									style="background-color: #7ca2c5; font-family: -webkit-body;">
									<h1 style="font-size: initial; font-family: auto;">Item
										Table</h1>
									<form action="insert" method="get">
										<a href="Add.html" class="btn btn-primary btn-sm"
											role="button" id="button1">Add Item</a> <a href="Update.html"
											class="btn btn-primary btn-sm" role="button" id="button2">Update
											Item</a> <a href="delete.html" class="btn btn-primary btn-sm"
											role="button3" id="button3">Delete Item</a>

									</form>
									<br>
									<br>
									<th>ItemID</th>
									<th>Name</th>
									<th>Description</th>
									<th>Category</th>
									<th>Price</th>
									<th>Available Quantity</th>
									<th>Place Order</th>
								</tr>
							</thead>
							<tbody>
								<%
									try {
										ArrayList<Item> stockList = display.getStock();
										for (int i = 0; i < stockList.size(); i++) {
											Item item = (Item) stockList.get(i);
								%>
								<tr>
									<td scope="row"><%=item.getItemID()%></td>
									<td scope="row"><%=item.getName()%></td>
									<td scope="row" class="w-25 p-3"><%=item.getDescription()%></td>
									<td scope="row"><%=item.getCategory()%></td>
									<td scope="row">Rs <%=item.getPrice()%> per ltr
									</td>
									<td scope="row"><%=item.getAvalable()%></td>

									<td scope="row"><a href="Order.html"
										class="btn btn-primary btn-sm" role="button" id="button1">Order</a>
									<td>
								</tr>
								<%
									}

									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								%>
							</tbody>


						</table>

					</div>
				</div>
			</div>
		</div>
		<br> <br>

		<div class="container">
			<div class="jumbotron">
				<div class="card">
					<h5 class="card-header">Order Management</h5>
					<div class="card-body">
						<table class="table table-striped">
							<thead>
								<tr
									style="background-color: #7ca2c5; font-family: -webkit-body;">
									<h1 style="font-size: initial; font-family: auto;">Order
										details</h1>
									<th>OrderID</th>
									<th>ItemID</th>
									<th>Item Name</th>
									<th>Item Category</th>
									<th>Email</th>
									<th>Address</th>
									<th>Quantity</th>
									<th>TotalAmount</th>
									<th>Status</th>
									<th>Update Status</th>
								</tr>
							</thead>
							<tbody>
								<%
									try {

										ArrayList<Order> orderlist = display.getOrderDetails();
										for (int i = 0; i <= orderlist.size(); i++) {
											Order order = (Order) orderlist.get(i);
								%>
								<tr>
									<td scope="row"><%=order.getOrderid()%></td>
									<td scope="row"><%=order.getItemid()%></td>
									<td scope="row"><%=order.getItemName()%></td>
									<td scope="row"><%=order.getCategory()%></td>
									<td scope="row"><%=order.getEmail()%></td>
									<td scope="row"><%=order.getAddress()%></td>
									<td scope="row"><%=order.getQuantity()%></td>
									<td scope="row"><%=order.getTotalamt()%></td>
									<td scope="row"><%=order.getStatus()%></td>
									<td scope="row"><a href="update.html"
										class="btn btn-primary btn-sm" role="button" id="button1">Update</a></td>

								</tr>
								<%
									}

									}

									catch (Exception e) {
										e.printStackTrace();
									}
								%>
							</tbody>
						</table>

					</div>
				</div>

			</div>
		</div>

	</div>

</body>
</html>