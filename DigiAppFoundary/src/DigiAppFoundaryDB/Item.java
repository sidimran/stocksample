package DigiAppFoundaryDB;

public class Item {

	private int ItemID;
	private String name;
	private String Description;
	private String Category;
	private double price;
	private int avalable;

	public Item() {
		
	}
	
	public Item(int ItemID, String name, String Description, String Category, double price, int avalable) {
		this.ItemID = ItemID;
		this.name = name;
		this.Description = Description;
		this.Category = Category;
		this.price = price;
		this.avalable = avalable;

	}

	public int getItemID() {
		return ItemID;
	}

	public void setItemID(int itemID) {
		ItemID = itemID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getAvalable() {
		return avalable;
	}

	public void setAvalable(int avalable) {
		this.avalable = avalable;
	}
}