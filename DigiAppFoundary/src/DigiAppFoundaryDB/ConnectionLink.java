package DigiAppFoundaryDB;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionLink {

	static Connection con = null;

	public static Connection Link() {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/DigiAppFoundary?user=root&password=root";
			con = DriverManager.getConnection(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;

	}

}
