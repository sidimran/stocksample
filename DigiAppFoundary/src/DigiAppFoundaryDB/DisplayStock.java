package DigiAppFoundaryDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DisplayStock {
	static Connection con = null;

	public ArrayList<Item> getStock() throws SQLException {

		con = ConnectionLink.Link();
		String url = "select * from Itemtable";
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(url);

		ArrayList<Item> al = new ArrayList<Item>();

		try {

			while (rs.next()) {
				Item item = new Item();

				item.setItemID(rs.getInt(1));
				item.setName(rs.getString(2));
				item.setDescription(rs.getString(3));
				item.setCategory(rs.getString(4));
				item.setPrice(rs.getDouble(5));
				item.setAvalable(rs.getInt(6));

				al.add(item);
			}
			return al;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return al;

	}

	public ArrayList<Order> getOrderDetails() throws SQLException {

		con = ConnectionLink.Link();
		String url = "select * from OrderTable";
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(url);

		ArrayList<Order> al = new ArrayList<Order>();

		try {

			while (rs.next()) {
				Order order = new Order();
				order.setOrderid(rs.getInt(1));
				order.setItemid(rs.getInt(2));
				order.setEmail(rs.getString(3));
				order.setAddress(rs.getString(4));
				order.setQuantity(rs.getInt(5));
				order.setTotalamt(rs.getInt(6));
				order.setStatus(rs.getString(7));
				order.setItemName(rs.getString(8));
				order.setCategory(rs.getString(9));

				al.add(order);
			}
			return al;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return al;

	}

	public Integer getTotalCategories() throws SQLException {


		try {
			Integer count;
			con = ConnectionLink.Link();
			String url = "select count(distinct(Category)) from itemtable";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(url);
			
			if (rs.next()) {
				count = rs.getInt(1);
				return count;
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Integer getTotalItems() throws SQLException {
        
		try {
			con = ConnectionLink.Link();
			String url = "select count(distinct(itemID)) from itemtable";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(url);

			if (rs.next()) {
				Integer count = rs.getInt(1);
				return count;
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<String> getMaxItems() throws SQLException {

		try {

			ArrayList<String> itemList = new ArrayList<String>();
			con = ConnectionLink.Link();
			String url = "select Category, Name, QuantitySold from itemtable where QuantitySold = (Select Max(QuantitySold) from itemtable)";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(url);

			if (rs.next()) {
				String category = rs.getString(1);
				String name = rs.getString(2);

				itemList.add(category);
				itemList.add(name);
				return itemList;
				
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
