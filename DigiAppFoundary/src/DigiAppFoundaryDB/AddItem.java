package DigiAppFoundaryDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AddItem {

	PreparedStatement pst = null;

	public String insert(int ItemID, String Name, String Description, String Category, double price,
			int AvailableQuantity) throws SQLException {

		Connection con = null;
		String table = "";
		try {

			con = ConnectionLink.Link();
			String query = "insert into Itemtable values (?,?,?,?,?,?)";
			pst = con.prepareStatement(query);
			pst.setInt(1, ItemID);
			pst.setString(2, Name);
			pst.setString(3, Description);
			pst.setString(4, Category);
			pst.setDouble(5, price);
			pst.setInt(6, AvailableQuantity);
			pst.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return table;
	}
}
