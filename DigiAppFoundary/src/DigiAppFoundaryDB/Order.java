package DigiAppFoundaryDB;

public class Order {

	private int orderid;
	private int itemid;
	private String email;
	private String address;
	private int quantity;
	private int totalamt;
	private String status;
	private String itemName;
	private String category;

	public Order() {

	}

	public Order(int orderid, int itemid, String email, String address, int quantity, int totalamt, String status,String itemName,String category ) {
		super();
		this.orderid = orderid;
		this.itemid = itemid;
		this.email = email;
		this.address = address;
		this.quantity = quantity;
		this.totalamt = totalamt;
		this.status = status;
		this.itemName = itemName;
		this.category = category;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public int getItemid() {
		return itemid;
	}

	public void setItemid(int itemid) {
		this.itemid = itemid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(int totalamt) {
		this.totalamt = totalamt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
