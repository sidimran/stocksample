package DigiAppFoundaryServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DigiAppFoundaryDB.AddItem;

public class AddServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	super.doGet(req, resp);
		
		PrintWriter out = resp.getWriter();
		
		int Itemid =Integer.parseInt(req.getParameter("Itemid"));
		String name = req.getParameter("name");
		String description = req.getParameter("description");
		String category = req.getParameter("category");
		double price = Double.parseDouble(req.getParameter("price"));
		int availablequantity = Integer.parseInt(req.getParameter("availablequantity"));
		
		AddItem insert = new AddItem();
		
		try {
			
			String table = insert.insert(Itemid, name, description, category, price, availablequantity);
			out.print(table);
		}
		catch(Exception e ) {
			e.printStackTrace();
		}
		
		
		
	}
	
	

}
